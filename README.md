# [Semantic3D](http://www.semantic3d.net/view_dbase.php?chl=2) preprocessed data

Created by [Xavier Roynard](https://www.researchgate.net/profile/Xavier_Roynard) from [NPM3D team](http://caor-mines-paristech.fr/fr/recherche/3d-modeling/) of Centre for Robotics of Mines ParisTech. 

----------------------------------------------------------------

![Exemple de Segmentation et Classification](data_example.jpg)

----------------------------------------------------------------

This repository contains preprocessed files of _reduced-8_ benchmark from dataset [Semantic3D](http://www.semantic3d.net/view_dbase.php?chl=2) used for example in [ms_deepvoxscene repository](https://github.com/xroynard/ms_deepvoxscene) 




